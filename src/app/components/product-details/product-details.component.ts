import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { Product } from 'src/app/models/product.model';
import { ProductService } from 'src/app/services/product.service';


@Component({
    selector: 'app-product-details',
    templateUrl: './product-details.component.html',
    styleUrls: ['./product-details.component.css']
  })

export class ProductDetailsComponent implements OnInit {

  constructor(private route: ActivatedRoute, private router: Router, private productService : ProductService) {

  }

  products ?: Product[];

  detailsProduit : Product = {
    id : '',
    image : '',
    productName : '',
    productPrice : '',
    available : false,
    description : ''
  };
  
    ngOnInit(): void {
      this.products = JSON.parse(localStorage.getItem("products"));
      this.getProductFromWebService(this.route.snapshot.params.id);
    }

    getProduct(id : string) : void {
      this.detailsProduit = this.products.filter(p => p.id === id) [0];
    }

    getProductFromWebService(id : string) : void {
       this.productService.getProductById(id)
       .subscribe(
         data => {
          this.detailsProduit = data;
         },
         error => {
           console.log(error);
         }
       )
    }  

}