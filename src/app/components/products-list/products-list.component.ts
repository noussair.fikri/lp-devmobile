import { Component, OnInit } from '@angular/core';
import { Product } from 'src/app/models/product.model';
import { ProductService } from 'src/app/services/product.service';

@Component({
    selector: 'app-products-list',
    templateUrl: './products-list.component.html',
    styleUrls: ['./products-list.component.css']
  })

export class ProdutsListComponent implements OnInit {

  constructor(private productService: ProductService) {

  }

  products ?: Product[];

  ngOnInit(): void {

      if(localStorage.getItem("products") === null) {
        this.products = 
            [{
              "id":"60088da4f0875903e86cefbb",
              "image": "https://cdn.pixabay.com/photo/2019/02/25/16/46/breakfast-4020028_960_720.jpg",
              "productName": "Amandes",
              "productPrice": 20,
              "description": "L'amande est le fruit de l'amandier (Prunus dulcis) (famille des Rosacées). L’amande est une graine riche en lipide (54 %), et en particulier en d’acide oléique et linoléique, un oméga-6. Elle est aussi remarquablement riche en protéines (22 %), quoique dépourvue de certains acides aminés essentiels comme la méthionine et la lysine.",
              "available": true,
            },{
              "id": "600f1acb03134d107d7e0f90",
              "image": "https://media.istockphoto.com/photos/single-wooden-cello-on-a-white-background-picture-id95655137",
              "productName": "Violon",
              "productPrice": 400,
              "description": "A single wooden cello on a white background stock photo",
              "available": false,
            },{
              "id":  "600f1bd703134d107d7e0f91",
              "image": "https://media.istockphoto.com/photos/lunettes-de-soleil-picture-id971507844?k=6&m=971507844&s=612x612&w=0&h=Q9mTlMPu85wKIbPNixLSrpTxg0dLrM2i_gFMzDjA-So=",
              "productName": "Lunettes de soleil",
              "productPrice": 350,
              "description": "Lunettes Soleil Pictures, Images and Stock Photos",
              "available": true,
            }];
        localStorage.setItem("products", JSON.stringify(this.products));
      }
      else {
        this.products = JSON.parse(localStorage.getItem("products"));
      }

  }

  resetProductsListOffline() : void {
    this.products = 
            [{
              "id":"60088da4f0875903e86cefbb",
              "image": "https://cdn.pixabay.com/photo/2019/02/25/16/46/breakfast-4020028_960_720.jpg",
              "productName": "Amandes",
              "productPrice": 20,
              "description": "L'amande est le fruit de l'amandier (Prunus dulcis) (famille des Rosacées). L’amande est une graine riche en lipide (54 %), et en particulier en d’acide oléique et linoléique, un oméga-6. Elle est aussi remarquablement riche en protéines (22 %), quoique dépourvue de certains acides aminés essentiels comme la méthionine et la lysine.",
              "available": true,
            },{
              "id": "600f1acb03134d107d7e0f90",
              "image": "https://media.istockphoto.com/photos/single-wooden-cello-on-a-white-background-picture-id95655137",
              "productName": "Violon",
              "productPrice": 400,
              "description": "A single wooden cello on a white background stock photo",
              "available": false,
            },{
              "id":  "600f1bd703134d107d7e0f91",
              "image": "https://media.istockphoto.com/photos/lunettes-de-soleil-picture-id971507844?k=6&m=971507844&s=612x612&w=0&h=Q9mTlMPu85wKIbPNixLSrpTxg0dLrM2i_gFMzDjA-So=",
              "productName": "Lunettes de soleil",
              "productPrice": 350,
              "description": "Lunettes Soleil Pictures, Images and Stock Photos",
              "available": true,
            }];
        localStorage.setItem("products", JSON.stringify(this.products));
  }
    
  resetProductsListWebService() : void {
      this.productService.getProducts()
        .subscribe(
          data => {
            console.log(data);
            this.products = data;
          },
          error => {
            console.log(error);
          }
        );

  }
  
}