import { Component, OnInit } from '@angular/core';
import { Product } from 'src/app/models/product.model';
import { ProductService } from 'src/app/services/product.service';
import { ProdutsListComponent } from '../products-list/products-list.component';

@Component({
    selector: 'app-add-product',
    templateUrl: './add-product.component.html',
    styleUrls: ['./add-product.component.css']
  })

export class AddProductComponent implements OnInit {

    constructor(private productService: ProductService) {

    }
    
    product: Product = {
        id : '',
        image : '',
        productName : '',
        productPrice : '',
        available : false,
        description : ''
    }

    submitted = false;

    ngOnInit(): void {
        console.log(localStorage.getItem("products"));
    }
    
    addProduct(): void {

        const newProduct = {
            id : "GEN-"+JSON.parse(localStorage.getItem("products")).length + 1,
            image : this.product.image,
            productName : this.product.productName,
            productPrice : this.product.productPrice,
            available : this.product.available,
            description : this.product.description
        };

        console.log(newProduct);
        let products = JSON.parse(localStorage.getItem("products"));
        products.push(newProduct);
        localStorage.setItem("products", JSON.stringify(products));
    }

    addProductInWebService(): void { 

        const newProduct = {
            image : this.product.image,
            productName : this.product.productName,
            productPrice : this.product.productPrice,
            available : this.product.available,
            description : this.product.description
        };

        this.productService.addProduct(newProduct)
        .subscribe(
            response => {
              console.log(response);
              this.submitted = true;
            },
            error => {
              console.log(error);
            }
          );
    }

}