export class Product {
    id?: any;
    image ?: any;
    productName ?: any;
    productPrice ?: any;
    available ?: boolean;
    description ?: any;
}