import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AddProductComponent } from './components/add-product/add-product.component';
import { ProductDetailsComponent } from './components/product-details/product-details.component';
import { ProdutsListComponent } from './components/products-list/products-list.component';

const routes: Routes = [
  { path: 'addProduct', component: AddProductComponent},
  { path: 'productsList', component: ProdutsListComponent},
  { path: 'products/:id', component: ProductDetailsComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
