import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http"
import { Observable } from "rxjs"
import { Product } from "../models/product.model";

const baseUrl = 'http://192.168.100.23:8080/api';

@Injectable({
    providedIn: 'root'
})
export class ProductService {

    constructor(private http: HttpClient ) {

    }

    getProducts(): Observable<Product[]> {
        return this.http.get<Product[]>(baseUrl+"/products");
    }

    getProductById(id: any): Observable<Product> {
        return this.http.get<Product>(baseUrl+"/productsById/"+id);
    }

    addProduct(product: any): Observable<any> {
        return this.http.post(baseUrl+"/products", product);
    } 
    
    update(id: any, product: any): Observable<any> {
        return this.http.put(baseUrl+"/updateProduct/"+id, product);
    }

    delete(id: any): Observable<any> {
        return this.http.delete(baseUrl+"/"+id);
    }

}